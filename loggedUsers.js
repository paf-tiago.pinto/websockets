class LoggedUsers {
    constructor() {
        this.users = new Map();
    }

    addUserInfo(user, socketId) {
        let userInfo = this.users.get(user.id);
        if (!userInfo) {
            let userInfo = { user: user, socketIds: [socketId], activeSocketIds: [] };
            this.users.set(user.id, userInfo);
        } else {
            if (!userInfo.socketIds) {
                userInfo.socketIds = [];
            }
            userInfo.socketIds.push(socketId);
            this.users.set(user.id, userInfo);
        }
        return userInfo;
    }

    removeUserInfoById(userId) {
        let userInfo = this.getUserInfoById(userId);
        if (userInfo === null) {
            return null;
        }
        let cloneUserInfo = Object.assign({}, userInfo);
        this.users.delete(userId);
        return cloneUserInfo;
    }

    getUserInfoById(userId) {
        return this.users.get(userId);
    }

    getActiveSocketIdsById(userId) {
        let user = this.users.get(userId);
        return user ? user.activeSocketIds : null;
    }

    addActiveSocketIdsByUserId(userId, socketId) {
        let activeSocketIds = this.getActiveSocketIdsById(userId);
        activeSocketIds.push(socketId);
        return this.users.set(userId, { ...this.users.get(userId), activeSocketIds: activeSocketIds });
    }

    resetActiveSocketIdsByUserId(userId) {
        return this.users.set(userId, { ...this.users.get(userId), activeSocketIds: [] });
    }

    setSocketIdsById(userId) {
        let userInfo = this.getUserInfoById(userId);
        let socketIds = userInfo.socketIds.filter(socketId => userInfo.activeSocketIds.find(activeSocketId => activeSocketId == socketId));
        return this.users.set(userId, { ...this.users.get(userId), socketIds: socketIds, activeSocketIds: [] });
    }
}

module.exports = LoggedUsers;