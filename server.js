require('dotenv').config({ path: __dirname + '/.env' })
const PORT = process.env.PORT || 3000;

var app = require("http").createServer();
const io = require("socket.io")(app, {
  allowEIO3: true,
  cors: {
    origin: process.env.ORIGINS.split(";"),
    methods: ["GET", "POST"],
    credentials: true,
  },
});

var LoggedUsers = require("./loggedUsers.js");
let loggedUsers = new LoggedUsers();

app.listen(PORT);

io.on("connection", function (socket) {
  socket.on("userEnter", function (user) {
    if (user && user.id) {
      socket.join(user.type);
      let userInfo = loggedUsers.addUserInfo(user, socket.id);
      if (userInfo !== undefined && userInfo.socketIds && userInfo.socketIds.length > 1 && userInfo.activeSocketIds.length == 0) {
        loggedUsers.resetActiveSocketIdsByUserId(user.id);
        let socketIds = userInfo.socketIds;
        if (socketIds !== undefined && socketIds.length > 0) {
          socketIds.forEach(socketId => io.to(socketId).emit("isSessionActive"));
          setTimeout(function () {
            loggedUsers.setSocketIdsById(user.id);
          }, 1000);
        }
      }
    }
  });

  socket.on("userExit", function (user) {
    if (user && user.id) {
      socket.leave(user.type);
      loggedUsers.removeUserInfoById(user.id);
    }
  });

  socket.on("notification", function (userIds) {
    if (Array.isArray(userIds) && userIds.length > 0) {
      userIds.forEach((userId) => {
        let userInfo = loggedUsers.getUserInfoById(userId);
        if (userInfo !== undefined) {
          loggedUsers.resetActiveSocketIdsByUserId(userId);
          if (userInfo.socketIds && userInfo.socketIds.length > 0) {
            userInfo.socketIds.forEach(socketId => io.to(socketId).emit("isSessionActive"));
            setTimeout(function () {
              loggedUsers.setSocketIdsById(userId);
              setTimeout(function () {
                userInfo = loggedUsers.getUserInfoById(userId);
                if (userInfo !== undefined && userInfo.socketIds && userInfo.socketIds.length > 0) {
                  userInfo.socketIds.forEach((socketId, index) => io.to(socketId).emit("updateNotifications", index == 0));
                }
              }, 1000);
            }, 1000);
          }
        }
      });
    }
  });

  socket.on("notificationWithoutActive", function (userIds) {
    if (Array.isArray(userIds) && userIds.length > 0) {
      userIds.forEach((userId) => {
        let userInfo = loggedUsers.getUserInfoById(userId);
        if (userInfo !== undefined) {
          if (userInfo.socketIds && userInfo.socketIds.length > 0) {
            userInfo.socketIds.forEach((socketId) => {
              if (socketId !== socket.id) {
                io.to(socketId).emit("updateNotifications", false);
              }
            });
          }
        }
      });
    }
  });

  socket.on("usersUpdated", function () {
    io.sockets.to("User").emit("updateUsers");
  });

  socket.on("officesUpdated", function () {
    io.sockets.to("User").emit("updateOffices");
  });

  socket.on("processTypesUpdated", function () {
    io.sockets.to("User").emit("updateProcessTypes");
  });

  socket.on("servicePricesUpdated", function () {
    io.sockets.to("User").emit("updateServicePrices");
  });

  socket.on("sessionIsActive", function (userId) {
    let activeSocketIds = loggedUsers.getActiveSocketIdsById(userId);
    if (activeSocketIds) {
      loggedUsers.addActiveSocketIdsByUserId(userId, socket.id);
    }
  });

  socket.on("notificationsOpen", function (userId) {
    let userInfo = loggedUsers.getUserInfoById(userId);
    if (userInfo !== undefined && userInfo.socketIds && userInfo.socketIds.length > 1) {
      userInfo.socketIds.forEach((socketId) => {
        if (socketId !== socket.id) {
          io.to(socketId).emit("resetHeader");
        }
      });
      userInfo.socketIds.forEach(socketId => io.to(socketId).emit("resetHeader"));
    }
  });
});
